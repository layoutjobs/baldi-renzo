


/**************************************
    MAPA
*************************************/

function initialize() {

  // Exibir mapa;
  var myLatlng = new google.maps.LatLng(-23.095290, -47.222754);
  var mapOptions = {
    zoom: 17,
    center: myLatlng,
    panControl: false,
    // mapTypeId: google.maps.MapTypeId.ROADMAP
    mapTypeControlOptions: {
      mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']
    }
  }


  // Parâmetros do texto que será exibido no clique;
  var contentString = '<h2>Almeida Renzo & Baldi Advogados</h2>' +
  '<p>Rua das Orquídeas, 737 - Conj. 511, Ed. Office Business, Jardim Pompéia Indaiatuba/SP - CEP 13345-040</p>' +
  '<a href="https://goo.gl/maps/hnMmP4X2Lwu" target="_blank">clique aqui para mais informações</a>';
  var infowindow = new google.maps.InfoWindow({
      content: contentString,
      maxWidth: 700
  });


  // Exibir o mapa na div #mapa;
  var map = new google.maps.Map(document.getElementById("mapa"), mapOptions);


  // Marcador personalizado;
  var image = 'icon-map.png';
  var marcadorPersonalizado = new google.maps.Marker({
      position: myLatlng,
      map: map,
      icon: image,
      title: 'Almeida Renzo & Baldi Advogados',
      animation: google.maps.Animation.DROP
  });


//   // Exibir texto ao clicar no ícone;
  google.maps.event.addListener(marcadorPersonalizado, 'click', function() {
    infowindow.open(map,marcadorPersonalizado);
  });


  // Estilizando o mapa;
  // Criando um array com os estilos
  var styles = [
    {
      stylers: [
        { hue: "#ccc" },
        { saturation: -100 },
        { lightness: 10 },
        { gamma: 0 }
      ]
    },
    {
      featureType: "road",
      elementType: "geometry",
      stylers: [
        { lightness: 100 },
        { visibility: "simplified" }
      ]
    },
    {
      featureType: "road",
      elementType: "labels"
    }
  ];

  // crio um objeto passando o array de estilos (styles) e definindo um nome para ele;
  var styledMap = new google.maps.StyledMapType(styles, {
    name: "Mapa Style"
  });

  // Aplicando as configurações do mapa
  map.mapTypes.set('map_style', styledMap);
  map.setMapTypeId('map_style');

}


// Função para carregamento assíncrono
function loadScript() {
  var script = document.createElement("script");
  script.type = "text/javascript";
  script.src = "http://maps.googleapis.com/maps/api/js?key=AIzaSyDmjkEri5pEh98mYRj8Jq9b4XqK3LoU_6w&sensor=true&callback=initialize";
  document.body.appendChild(script);
}

window.onload = loadScript;
