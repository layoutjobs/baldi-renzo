﻿<!DOCTYPE html>
<html>
<head>
	<title>ARB | Almeida Renzo & Baldi Advogados</title>
	
	<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700|Roboto:300i,500" rel="stylesheet">
	
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="stylesheet" type="text/css" href="assets/css/main.css">
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>

	<script>
		var $doc = $('html, body');
		$('.scrollSuave').click(function() {
			$doc.animate({
				scrollTop: $( $.attr(this, 'href') ).offset().top
			}, 500);
			return false;
		});

	</script>
	
</head>

<body>


	<div class="container-box">
		<div class="container">
		  <nav class="navbar navbar-expand-lg fixed-top" role="navigation">
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample10" aria-controls="navbarsExample10" aria-expanded="false" aria-label="Toggle navigation">
				  <span class="navbar-toggler-icon"></span>
				</button>

				<div class="collapse navbar-collapse justify-content-md-center" id="navbarsExample10">
				  <ul class="navbar-nav">
					<li class="nav-item">
					  <a class="scrollSuave nav-link" href="#quem-somos">QUEM SOMOS</a>
					</li>
					<li class="nav-item">
					   <a class="nav-link" href="#atuacao">ÁREA DE ATUAÇÃO</a>
					</li>
					<li class="nav-item">
					  <a class="nav-link" href="#equipe">EQUIPE</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#contato">CONTATO</a>
					</li>	
					<li class="nav-item">
						<a style="color: #0096b7" class="nav-link" href="https://qlink.to/arbadv">ACESSO CLIENTE &nbsp;<img src="assets/img/area_restrita.png" alt="Ícone-Escudo"></a>
					</li>	
				  </ul>
				</div>
			</nav>
		</div>	
	</div>


	<div class="container-box">
		<div class="text-center">
		  <section class="banner-logo">
			<div class="container">
				<img class="logo" src="assets/img/logo.png">
			</div>
		  </section>
		</div>
	</div>	

	<div class="container-box">	
		<section class="banner-frase-georg">
			<div class="container">
				<div class="col-12 text-center">
					<div class="bloco-azul">
					<p>“O direito não é nada, além do mínimo ético.”</p>
					<span>Georg Jellinek</span>
					</div>
				</div>
			</div>
		</section>
	</div>	
	
    <div class="container-box">
      <section class="frase-advogados" id="quem-somos">
		  <div class="container">
			<p>A <strong>ALMEIDA RENZO & BALDI SOCIEDADE DE ADVOGADOS</strong> presta os mais variados serviços de natureza jurídica, especialmente para empresas de médio e grande porte nos setores de obras e infraestrutura, concessões públicas, empresas de investimentos, prestadoras de serviços, empreendedores, loteadores, e, incorporadores imobiliários.</p>
		  </div>
	  </section>
	</div>
	
	<div class="container-box">
		<section class="banner-assessoria">
			<div class="container">
				<div class="row">
					<div class="col-md-5">
						<div class="titulo-assessoria">	
						<span></span><br><br>  
						<p>A Assessoria jurídica para empresas de médio e grande porte.</p>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
  
	<div class="container-box">
		<section class="frase-clientes">
			<div class="container">
				<p class="icon-adv text-center"><img src="assets/img/icon-adv.png"></p><br>
				<p>Para a <strong>Almeida Renzo & Baldi</strong> cada cliente é único, por isto, o escritório prima pelo atendimento personalizado, sempre buscando uma perfeita sintonia entre a necessidade do cliente e os serviços prestados.  Para tanto, a  <strong>Almeida Renzo & Baldi</strong> dispõe de um quadro formado  por profissionais de nível sênior, o que lhe permite entregar aos seus clientes serviços jurídicos eficazes, com qualidade, e profundidade em seu ramo de atuação.</p>
			</div>
		</section>
	</div>
	
	<div class="container-box">
		<section class="banner-areas-atuacao" id="atuacao">
			<div class="container">
				<div class="row">
					<div class="col-md-5">
						<div class="titulo-areas-atuacao">
						<span></span><br><br>  
						<p>Áreas de atuação.</p>
						<span></span><br><br>
						</div>
					</div>
				</div>
			</div>	
		</section>
	</div>

	
	<div class="container-box">
	<div class="container">
		<section class="areas-atuacao">
		
		<div class="collapse-atuacao">
			<div class="clearfix"></div>
			  <small>Para mais detalhes de cada área 	de atuação, clique na seta azul para 	expandir.
			  </small>
			<div class="clearfix"></div>
			<p>
				<p class="title" data-toggle="collapse" href="#collapse-adm" role="button" aria-expanded="false" aria-controls="collapse">
				 <span></span> Administrativo e Regulatório
				</p>
				<hr />
			 </p>
			<div class="collapse" id="collapse-adm">
			  <div class="card card-body-text">
				Direito Administrativo, com foco em Contratações Públicas, Concessões e Parcerias Público-Privadas, e, Direito Regulatório em serviços públicos de infraestrutura.
			  </div>
			</div>

			  <p>
				<p class="title" data-toggle="collapse" href="#collapse-ambiental" role="button" aria-expanded="false" aria-controls="collapse">
				 <span></span> Ambiental e Sustentabilidade
				</a>
				<hr />
			  </p>
			<div class="collapse" id="collapse-ambiental">
			  <div class="card card-body-text">
				Direito Ambiental, com foco em ações preventivas, corretivas e mitigadoras em processos administrativos e judiciais de natureza ambiental. 
				Assessoria em projetos de sustentabilidade e acompanhamento em consultas técnicas, estudos e pareceres relacionados a temas socioambientais.

			  </div>
			</div>

			 <p>
				<p class="title" data-toggle="collapse" href="#collapse-contratos" role="button" aria-expanded="false" aria-controls="collapse">
				 <span></span> Contratos
				</p>
				<hr />
			  </p>
			<div class="collapse" id="collapse-contratos">
			  <div class="card card-body-text">
				Gestão de Contratos. Análise e revisão de instrumentos jurídicos.
				Assessoria à áreas internas da Companhia (Suprimentos, Comercial e Operacional) para fins de controle das principais obrigações pactuadas.

			  </div>
			</div>

			 <p>
				<p class="title" data-toggle="collapse" href="#collapse-imob" role="button" aria-expanded="false" aria-controls="collapse">
				 <span></span> Imobiliário
				</p>
				<hr />
			  </p>
			<div class="collapse" id="collapse-imob">
			  <div class="card card-body-text">
				Assessoria na formatação, estruturação e desenvolvimento de empreendimentos imobiliários.  
				Assessoria jurídica em desapropriações, servidões administrativas e judiciais.

			  </div>
			</div>

			 <p>
				<p class="title" data-toggle="collapse" href="#collapse-tribut" role="button" aria-expanded="false" aria-controls="collapse">
				 <span></span> Tributário
				</p>
				<hr />
			  </p>
			<div class="collapse" id="collapse-tribut">
			  <div class="card card-body-text">
				Planejamento e análise tributária empresarial. 
				Assessoria administrativa e judicial.

			  </div>
			</div>


			 <p>
				<p class="title" data-toggle="collapse" href="#collapse-consumidor" role="button" aria-expanded="false" aria-controls="collapse">
				 <span></span> Consumidor
				</p>
				<hr />
			  </p>
			<div class="collapse" id="collapse-consumidor">
			  <div class="card card-body-text">
				Análise, elaboração e revisão de contratos de consumo. 
				Orientação à empresas para o relacionamento com seus consumidores. 
				Atuação em defesa da empresa, nos processos perante órgãos de proteção ao consumidor.

			  </div>
			</div>
		</div>	
			
		<div class="clearfix"></div>
		<div class="container">
			<div class="row">
			<div class="col-md-4">
			  <div class="card card-01">
				<img class="card-img-top" src="assets/img/img-contratos.png" alt="">
				<div class="card-body">
				  <h4 class="card-title">Gestão de Contratos Jurídicos</h4>
					<p class="card-text">Gestão de Contratos. Análise e revisão de instrumentos jurídicos. Assessoria à áreas internas da Companhia (Suprimentos, Comercial e Operacional) para fins de controle das principais obrigações pactuadas.</p>
				</div>
			  </div>
			</div>

			<div class="col-md-4">
			  <div class="card card-01">
				<img class="card-img-top" src="assets/img/img-regulatorio.png" alt="">
				<div class="card-body">
				  <h4 class="card-title">Administrativo e Regulatório</h4>
					<p class="card-text">Direito Administrativo com foco em Contratações Públicas, Concessões e Parcerias Público-Privadas e Direito Regulatório em serviços públicos de infraestrutura.</p>
				</div>
			  </div>
			</div>

			<div class="col-md-4">
			  <div class="card card-01">
				<img class="card-img-top" src="assets/img/img-ambiental.png" alt="">
				<div class="card-body">
				  <h4 class="card-title">Ambiental e Sustentabilidade</h4>
					<p class="card-text">Centrado em ações preventivas e corretivas em processos administrativos e judiciais. Assessoria em projetos e acompanhamento em consultas técnicas, estudos e pareceres no âmbito socioambiental.</p>
				</div>
			  </div>
			</div>

			</div>
		</div>
	</section>
	</div>
	</div>

	
	<div class="container-box">
	  <section class="banner-knowhow">
	  	<div class="col-12">
		<div class="bloco-azul">
		  <p>Competência e know-how técnico.</p>
		</div>
		</div>
	  </section>
    </div>
	  
	<div class="container-box">
	<div class="container">
	  <section class="advogados-socios" id="equipe">
	  	<div class="row">
			<div class="col-sm">
				<div class="card card-01">
					<img class="card-img-top" src="assets/img/tiago.jpg" alt="">
					<div class="card-body">
					  <h4 class="card-title">Tiago Rodrigues Renzo</h4>
						<p class="subtitulo">Sócio</p>
						<p class="card-text">Advogado, especialista em Direito Tributário pela PUC/SP, MBA em Direito Empresarial pela FGV em curso, possui mais de 10 (dez) anos de experiência na área empresarial, especialmente no setor de engenharia e infra-estrutura, tendo por longo período exercido cargos de liderança em empresas de grande porte (Grupo Ecorodovias, antigo Grupo CIBE-BERTIN/EQUIPAV e Grupo Schincariol), e no setor público, como Secretário de Negócios Jurídicos do município de Salto.</p>
					</div>
				  </div>
			</div>
			<div class="col-sm">
				<div class="card card-01">
					<img class="card-img-top" src="assets/img/welica.jpg" alt="">
					<div class="card-body">
					  <h4 class="card-title">Wélica Almeida Renzo</h4>
					  <p class="subtitulo">Sócia</p>
						<p class="card-text">Advogada e consultora de empresas, sócia fundadora da ALMEIDA RENZO SOCIEDADE DE ADVOGADOS. Especialista em Direito Registral Imobiliário pelo IRIB – Instituto de Registro Imobiliário do Brasil, com mais de 10 (dez) anos de atuação na área empresarial, especialmente para empresas do setor imobiliário (loteadoras e incorporadoras) e em concessões públicas de saneamento básico.</p>
					</div>
				 </div>
			</div>
			<div class="col-sm">
				  <div class="card card-01">
					<img class="card-img-top" src="assets/img/bruno.jpg">
					<div class="card-body">
					  <h4 class="card-title">Bruno Martins Baldi</h4>
					  <p class="subtitulo">Sócio</p>
						<p class="card-text">Advogado, graduado e especialista em Direito Ambiental pela Faculdade de Direito de Itu, possui MBA em Direito e Economia da Regulação pela GVLaw/FGV-EESP, especialista em Gestão Empresarial pela Fundação Dom Cabral, experiência em docência universitária, com mais de 10 (dez) anos de experiência em Direito Público, tendo atuado em cargos de liderança nos setores de infraestrutura, especialmente em concessões rodoviárias e saneamento básico. Foi coordenador do comitê jurídico regulatório da Associação Brasileiro das Concessionárias Privadas de Saneamento Básico – ABCON.</p>
					</div>
				  </div>
			</div>
		</div>
	  </section>
	  </div>
    </div> 
	
	<section class="mapa">
		<div class="container-box">
    		<div id="mapa"></div>
    	</div>
    </section>	  	
  
	<div class="container-box">
			<section class="banner-footer" id="contato">
				<div class="container">
					<div class="row">
						<div class="col-md-6">
							<img style="margin-top: 0px;" class="logo-rodape" src="assets/img/logo-footer.png"><br><br>
							<a class="email" href="mailto:contato@almeidarenzo.com.br">contato@almeidarenzo.com.br</a>
						</div>
						<div class="col-md-6">
							<address class="endereco-rodape">
							<p>Telefone: +55 19 3199-2990</p>

							<p class="endereco">Rua das Orquídeas, 737 - Conj. 511, Ed. Office Business, Jardim Pompéia Indaiatuba/SP - CEP 13345-040</p>
							<br>
							<br>
							</address>
						</div>
					</div>
					</div>
			</section>
	</div>
	
	<div class="container-box">
		<footer>
			
			<p>ARB Almeida Renzo & Baldi Sociedade de Advogados. Todos os direitos reservados.</p>
				 <div class="redes-sociais"> 
				  <a href="https://pt-br.facebook.com/almeidarenzo/" target="_blank"><img class="icon-social" src="assets/img/icon-facebook.png"></a>
				  <a href="https://www.linkedin.com/company/almeida-renzo-baldi/" target="_blank"><img class="icon-social" src="assets/img/icon-linkedin.png"></a>
				</div>
		</footer>
	</div>



<script type="text/javascript" src="assets/js/main.js"></script>
<script type="text/javascript" src="assets/js/util.js"></script>
<script type="text/javascript" src="assets/js/jquery-3.1.1.min.js"></script>

</body>
</html>