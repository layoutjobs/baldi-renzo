function loadGoogleMaps() {
    var script = document.createElement('script');
    script.src = '//maps.googleapis.com/maps/api/js?key=AIzaSyBD2FEfHmFdwfaA6e0AoP7FLKjoshyEjTE&callback=initializeGoogleMaps';
    document.body.appendChild(script);
}

function initializeGoogleMaps() {
    var lat = -22.9338354;
    var lng = -47.0695987;
    var styles = [
        {
            stylers: [
                { saturation: -100 }
            ]
        },{
            featureType: 'road',
            elementType: 'geometry',
            stylers: [
                { lightness: 10 },
                { visibility: 'simplified' }
            ]
        },{
            featureType: 'road',
            elementType: 'labels',
            stylers: [
                { visibility: 'off' }
            ]
        }
    ];

    var styledMap = new google.maps.StyledMapType(styles, { name: 'Styled Map' });

    var mapOptions = {
        zoom: 17,
        scrollwheel: false,
        disableDefaultUI: true,
        zoomControl: true,
        streetViewControl: true,
        center: new google.maps.LatLng(lat, lng),
        mapTypeControlOptions: {
            mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'mapStyle']
        }
    };

    var map = new google.maps.Map(document.getElementById('map'), mapOptions);

    map.mapTypes.set('mapStyle', styledMap);
    map.setMapTypeId('mapStyle');

    var marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat, lng),
        map: map,
        title: 'RIP Editores',
        icon: 'assets/images/map-marker.png',
        url: 'https://goo.gl/maps/oXPwpAJHW2D2',
        animation: google.maps.Animation.DROP
    });

    if (marker.url) {
        google.maps.event.addListener(marker, 'click', function() {
            window.open(marker.url, '_blank');
        });
    }
}

loadGoogleMaps();